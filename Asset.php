<?php
namespace skewer\ext\fotorama;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle{

    public $sourcePath = '@vendor/skewer_team/fotorama/web/';

    public $css = [
        'fotorama.css'
    ];

    public $js = [
        'fotorama.js',
        'fotoramaInit.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];


}
